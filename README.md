Password-Chameleon-Lite
=======================

Password Chameleon (http://www.passwordchameleon.com) with additional features

* Can hide passwords
* Can clear all fields
